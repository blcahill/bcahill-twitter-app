<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Lite Weight Twitter</title>
	<link rel="stylesheet" href="{{asset('css/welcome.css')}}">
	<link rel="stylesheet" href="{{asset('css/home.css')}}">
	<link rel="stylesheet" href="{{asset('css/main.css')}}">
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<header>
	<div class="content-wrapper">
		<i class="fa fa-twitter fa-3x"></i>
		<a href="{{ URL::to('/') }}" class="home-link"><span class="diet">Twitter</span>Lite</a>
		<p class="saying">twitter without the fuss</p>
	</div>
</header>
<body>
	<div class="page-container">
		<div class="content-wrapper">


		@yield('content')

		</div>
	</div>
</body>
<footer>

</footer>
</html>