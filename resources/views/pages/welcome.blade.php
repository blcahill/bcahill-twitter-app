@extends('master')

@section('content')

<ul class="my-nav">
  <li class="action login"><a href="{{URL::to('twitter/login')}}">Log Into Twitter</a></li>
  <li class="action create"><a href="https://twitter.com/">Create A Twitter Account</a></li>
</ul>

@stop
