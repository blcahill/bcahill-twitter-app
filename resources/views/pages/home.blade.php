@extends('master')

@section('content')
	
	<div class="user-info">
		<div class="my-wrapper">
			<div class="fake-header">
				<div class="user-image" style="background-image: url({{$userpoop['image']}});">
				</div>
				<h2 class="my-header">{{$userpoop["name"]}}</h2>
				<a href="{{URL::to('twitter/logout')}}" class="btn btn-default my-button">Log Out</a>
			</div>
			<h3 class="handle">@</h3>
			<h3 class="handle">{{$userpoop["twitter_handle"]}}</h3>
			
			<p class="bio">{{$userpoop["description"]}}</p>
			<p class="location">{{$userpoop["location"]}}</p>
		</div>
	</div>

	<div class="my-form">
		<div class="my-wrapper">
		<h2>Tweet Your Thoughts</h2>
		<form method="POST" action="{{ action('PagesController@tweet') }}">
		{!! csrf_field() !!}
		  <div class="form-group">
		   <textarea class="form-control" rows="5" id="comment" name="tweet">Twitter stuff..</textarea>
		  </div>
		  <button type="submit" class="btn btn-default my-button">Tweet It!</button>
		</form>
		</div>
	</div>

	<div class="user-tweets">
		<div class="my-wrapper">
		<h2>Your Recent Tweets</h2>
			<div class="tweet-content">
				<ul class="tweets">
					@foreach ($tweet_texts as $tweet_text)
					<li class="tweet">
						<p class="tweet-content"><?php echo $tweet_text ?></p>	
					</li>
					@endforeach
				</ul>
<!-- 				<ul class="timestamps">
					@foreach ($timestamps as $timestamp)
					<li class="timestamp">
						<p class="tweet-content">{{$timestamp}}</p>
					</li>
					@endforeach	
				</ul> -->
			</div>
		</div>
	</div>

	<div class="timeline">
		<div class="my-wrapper">
		<h2>Your Timeline</h2>
		<ul class="home-tweets">
			@foreach ($homefeed as $item)
			<li class="home-tweet">
				<div class="icon" style="background-image: url({{$item['user']['profile_image_url']}});"></div>
				<div class="tweet-content">
				<p class="handle">@</p><p class="handle">{{$item["user"]["screen_name"]}}</p>
				<p class="handle">{{$item["text"]}}</p>
				</div>
			</li>
			@endforeach	
		</ul>
	</div>
	</div>

@stop




