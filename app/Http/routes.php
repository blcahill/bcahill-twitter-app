<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    return view('pages.welcome');
});

Route::get('home', function(){
	return view('pages.home');
});



// Route::get('/', function () {
//     return Twitter::getUserTimeline(['screen_name' => 'bridgey__boo', 'count' => 20, 'format' => 'json']);
// });

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {


    Route::get('/', function () {
        return view('pages.welcome');
    });

    Route::get('twitter/login', 'PagesController@login')->name('twitter.login');
    Route::get('twitter/callback', 'PagesController@callback')->name('twitter.callback');
    Route::get('twitter/error', 'PagesController@error')->name('twitter.error');
    Route::get('twitter/logout', 'PagesController@logout')->name('twitter.logout');
    Route::get('home', 'PagesController@home');
    Route::post('tweet', 'PagesController@tweet');

});
