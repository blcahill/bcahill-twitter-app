<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Twitter;
use Session;
use Redirect;
use Illuminate\Support\Facades\Input;

class PagesController extends Controller
{
    public function login(){
        // your SIGN IN WITH TWITTER  button should point to this route
        $sign_in_twitter = true;
        $force_login = false;

        // Make sure we make this request w/o tokens, overwrite the default values in case of login.
        Twitter::reconfig(['token' => '', 'secret' => '']);
        $token = Twitter::getRequestToken(route('twitter.callback'));


        if (isset($token['oauth_token_secret']))
        {
            $url = Twitter::getAuthorizeURL($token, $sign_in_twitter, $force_login);
            Session::put('oauth_state', 'start');
            Session::put('oauth_request_token', $token['oauth_token']);
            Session::put('oauth_request_token_secret', $token['oauth_token_secret']);
            Session::save();
       
            return Redirect::to($url);

        }
        
        return Redirect::route('twitter.error');
    }

	public function callback() {
        // You should set this route on your Twitter Application settings as the callback
        // https://apps.twitter.com/app/YOUR-APP-ID/settings

        if (Session::has('oauth_request_token'))
        {
            $request_token = [
                'token'  => Session::get('oauth_request_token'),
                'secret' => Session::get('oauth_request_token_secret'),
            ];
    
            Twitter::reconfig($request_token);


            $oauth_verifier = false;

            if (Input::has('oauth_verifier'))
            {
                $oauth_verifier = Input::get('oauth_verifier');
            }

            // getAccessToken() will reset the token for you
            $token = Twitter::getAccessToken($oauth_verifier);

            if (!isset($token['oauth_token_secret']))
            {
                return Redirect::route('twitter.login')->with('flash_error', 'We could not log you in on Twitter.');
            }

            $credentials = Twitter::getCredentials();
            // $thing = get_object_vars($credentials);
            // dd($thing);
            // dd($credentials);

            if (is_object($credentials) && !isset($credentials->error))
            {
                // $credentials contains the Twitter user object with all the info about the user.
                // Add here your own user logic, store profiles, create new users on your tables...you name it!
                // Typically you'll want to store at least, user id, name and access tokens
                // if you want to be able to call the API on behalf of your users.
                // $thing= [];
                $array =  (array) $credentials;
                $id = $array["id"];
                $description = $array["description"];
				$name = $array["name"];
				$twitter_handle = $array["screen_name"];
				$image  = $array["profile_image_url"];
                $location = $array["location"];

       			
            	$user = ["id" => $id, 
                        "name" => $name, 
                        "twitter_handle" => $twitter_handle,
                        "image" => $image, 
                        "description" => $description,
                        "location" => $location
                        ];

                Session::put('access_token', $token);

                $tweets = Twitter::getUserTimeline(['screen_name' => $user['twitter_handle'], 'count' => 20, 'format' => 'json']);
                $json_tweets = json_decode($tweets, true);

                Session::put('userpoop', $user);
                Session::put('tweets', $json_tweets);
                return Redirect::to('/home');
            }

            return Redirect::route('twitter.error')->with('flash_error', 'Crab! Something went wrong while signing you up!');
        }
    }

	public function error(){
		return Redirect::to('/')->with('flash_notice', 'Something\'s Wrong!');
	}

	public function home(){

		$user = Session::get('userpoop');
		$tweets = Session::get('tweets');

        // dd($tweets);

        $timestamps =[];
        if($tweets){
            foreach ($tweets as $tweet) {
                $time = Twitter::ago($tweet["created_at"]);
                array_push($timestamps, $time);
            }
        }

        $linkify =[];
        if($tweets){
            foreach ($tweets as $tweet) {
                $thing = Twitter::linkify($tweet);
                array_push($linkify, $thing);

            }
        }

        // dd($linkify);
        // $linkify = htmlentities($linkify);
        // dd($linkify);
        Session::put('tweets_text', $linkify);
        

        $json_home = Twitter::getHomeTimeline(['count' => 20, 'format' => 'json']);
        $home_feed = json_decode($json_home, true);

        // $linked_home = [];
        // if($home_feed){
        //     foreach ($home_feed as $home_feed_tweets) {
        //         $thing = Twitter::linkify($home_feed_tweets);
        //         array_push($linked_home, $thing);

        //     }
        // }
        // dd($linked_home);
        // Session::put('feed', $linked_home);

        if (Session::get('userpoop')){
		return view('pages.home')->with([
        "userpoop"=> $user,
         "tweets"=> $tweets, 
         "timestamps" => $timestamps,
          "homefeed" => $home_feed,
          "tweet_texts" => $linkify]);
        }

        return Redirect::to('/');
	}

    public function logout() {
        Session::forget('access_token');
        Session::flush();
        return Redirect::to('/')->with('flash_notice', 'You\'ve successfully logged out!');
    }


    public function tweet(Request $request){
    	// $input = Input::all();

        $tweet = $request->input('tweet');
        // dd($tweet);

        $this->validate($request, [
               'tweet' => 'required|max:140',
           ]);
        $tweet = $request->input('tweet');
        
        $user = Session::get('userpoop');
        $new = Twitter::postTweet(['status' => $tweet, 'format' => 'json']);
        $tweets = Twitter::getUserTimeline(['screen_name' => $user['twitter_handle'], 'count' => 20, 'format' => 'json']);
        $json_tweets = json_decode($tweets, true);
        Session::put('tweets', $json_tweets);

        return Redirect::to('/home');
    }
}
